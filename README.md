# Corestore Metadata Db

Provides an interface to a simple Sqlite3 database which keeps track of hypercores stored in a corestore.
It can be used to persist information between corestore sessions,
since a corestore does not know which hypercores it contains on disk.

Note that this module is not concerned with whether the corestores and cores it
stores actually exist. This is the responsibility of the program which uses this module.

Warning: this library is still in alfa, and there are likely to be breaking changes before it reaches v1.0.0.

## Install
`npm add corestore-metadata-db`

## Usage
```
const DbInterface = require('corestore-metadata-db')

const dbInterface = DbInterface.initFromConnectionStr(':memory:')
const key = 'a'.repeat(64)
const key2 = 'b'.repeat(64)
const corestorePath = './my-corestore'

dbInterface.addCore({ name: 'myCore', key, corestorePath })
dbInterface.addCore({ name: 'myCore2', key: key2, corestorePath })

const coreInfo = dbInterface.getCoreByKey(key)
console.log(coreInfo)
/*
  DbCore {
    id: 1,
    name: 'myCore',
    key: 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
    corestorePath: './my-corestore'
  }
*/

const coreInfos = dbInterface.getCoresOfCorestore(corestorePath)
console.log(coreInfos)

/*
[
  DbCore {
    id: 1,
    name: 'myCore',
    key: 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
    corestorePath: './my-corestore'
  },
  DbCore {
    id: 2,
    name: 'myCore2',
    key: 'bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb',
    corestorePath: './my-corestore'
  }
]
*/


 
