const { expect } = require('chai')

const DbInterface = require('.')
const DbCore = require('./lib/core-in-db')

describe('Db-interface tests', function () {
  let dbInterface
  let key

  this.beforeEach(function () {
    dbInterface = DbInterface.initFromConnectionStr(':memory:')
    key = 'a'.repeat(64)
  })

  it('Can insert a core in the db', function () {
    dbInterface.addCore({
      name: 'testcore',
      key,
      corestorePath: './somepath'
    })

    const createdCore = dbInterface.getCoreByKey(key)
    const expectedCore = new DbCore({
      id: 1,
      name: 'testcore',
      key,
      corestorePath: './somepath'
    })
    expect(JSON.stringify(createdCore)).to.equal(JSON.stringify(expectedCore))
  })

  it('throws when adding 2 cores with the same key', function () {
    dbInterface.addCore({
      name: 'testcore',
      key,
      corestorePath: './somepath'
    })
    expect(() =>
      dbInterface.addCore({
        name: 'otherName',
        key,
        corestorePath: './somepath'
      })
    ).to.throw('UNIQUE constraint failed: core.key')
  })

  it('Can get the cores of a corestore', function () {
    const corestoreLoc = './somepath'

    dbInterface.addCore({
      name: 'testcore',
      key,
      corestorePath: corestoreLoc
    })
    dbInterface.addCore({
      name: 'testcore2',
      key: 'b'.repeat(64),
      corestorePath: corestoreLoc
    })
    dbInterface.addCore({
      name: 'testcore2',
      key: 'c'.repeat(64),
      corestorePath: './another_loc'
    })

    const cores = dbInterface.getCoresOfCorestore(corestoreLoc)

    expect(cores.length).to.equal(2)
    expect(cores[0].corestorePath).to.equal('./somepath')
    expect(cores[1].corestorePath).to.equal('./somepath')
  })

  describe('It can get a core by its key', function () {
    this.beforeEach(function () {
      dbInterface.addCore({
        name: 'testcore',
        key,
        corestorePath: './somepath'
      })
    })

    it('Can get a core by its str key', function () {
      const core = dbInterface.getCoreByKey(key)

      expect(core.key).to.equal(key)
      expect(core.name).to.equal('testcore')
      expect(core.corestorePath).to.equal('./somepath')
    })

    it('Can get a core by its buffered key', function () {
      const core = dbInterface.getCoreByKey(Buffer.from(key, 'hex'))

      expect(core.key).to.equal(key)
      expect(core.name).to.equal('testcore')
      expect(core.corestorePath).to.equal('./somepath')
    })
  })
})
