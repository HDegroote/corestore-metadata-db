const b4a = require('b4a')
const sqlite3 = require('better-sqlite3')

const DbCore = require('./lib/core-in-db')

function getKeyAsStr (key) {
  const res = typeof key === 'string' ? key : b4a.toString(key, 'hex')
  return res
}

class DbInterface {
  RELEVANT_COLUMNS = 'id, name, key, corestore_path'

  constructor (db) {
    this.db = db
    this.initTables()
  }

  initTables () {
    const command = this.db.prepare(
      [
        'CREATE TABLE IF NOT EXISTS core (',
        'id INTEGER PRIMARY KEY,',
        'name TEXT NOT NULL,',
        'key TEXT UNIQUE NOT NULL,',
        'corestore_path TEXT NOT NULL,',
        'corestore_namespace TEXT',
        ');'
      ].join(' ')
    )
    command.run()
  }

  addCore ({ name, key, corestorePath }) {
    key = getKeyAsStr(key)

    const command = this.db.prepare(
      'INSERT INTO core (name, key, corestore_path) VALUES (?, ?, ?);'
    )
    command.run(name, key, corestorePath)
  }

  getCoreByKey (key) {
    key = getKeyAsStr(key)
    const command = this.db.prepare(
      `SELECT ${this.RELEVANT_COLUMNS} FROM core WHERE key = ?;`
    )
    const res = command.get(key)

    return res ? this.createCoreFromDbRow(res) : res
  }

  getCoresOfCorestore (corestorePath) {
    const command = this.db.prepare(
      `SELECT ${this.RELEVANT_COLUMNS} FROM core WHERE corestore_path=?;`
    )
    const rows = command.all(corestorePath)

    const cores = rows.map((row) => this.createCoreFromDbRow(row))
    return cores
  }

  createCoreFromDbRow ({ id, name, key, corestore_path }) { // eslint-disable-line camelcase
    return new DbCore({
      id,
      name,
      key,
      corestorePath: corestore_path // eslint-disable-line camelcase
    })
  }

  static initFromConnectionStr (connectionStr) {
    const db = sqlite3(connectionStr)
    return new DbInterface(db)
  }
}

module.exports = DbInterface
