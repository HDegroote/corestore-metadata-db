class DbCore {
  constructor ({ id, name, key, corestorePath }) {
    this.id = id
    this.name = name
    this.key = key
    this.corestorePath = corestorePath
  }
}

module.exports = DbCore
